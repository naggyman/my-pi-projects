#!/usr/bin/python
# Code from Adafruit and Morgan French-Stagg

from Adafruit_PWM_Servo_Driver import PWM
import time
import sys

print('Continuous Servo Driver - Morgan French-Stagg')

pwm = PWM(address=0x40, debug=True) # PWM Address

cont1Channel = 0 # Address on PWM Driver of continuous servo (0 to 15)
cont2Channel = 1 # Address on PWM Driver of continuous servo (0 to 15)

contForward = 600  # Forwards pulse (out of 4096)
contBack = 150 # Backwards pulse (out of 4096)
contStop = 0 # No pulse

direction = str(sys.argv[1]) # Get direction from Argument (f,b)

def setServoPulse(channel, pulse):
  pulseLength = 1000000                   # 1,000,000 us per second
  pulseLength /= 50                       # 50 Hz
  print "%d us per period" % pulseLength
  pulseLength /= 4096                     # 12 bits of resolution
  print "%d us per bit" % pulseLength
  pulse *= 1000
  pulse /= pulseLength
  pwm.setPWM(channel, 0, pulse)

pwm.setPWMFreq(50) # Set frequency to 50 Hz

if direction == 'f':
    pwm.setPWM(cont1Channel, 0, contForward)
    pwm.setPWM(cont2Channel, 0, contBack)
    print('Set continuous servos to forward')
elif direction == 'b':
    pwm.setPWM(cont1Channel, 0, contBack)
    pwm.setPWM(cont2Channel, 0, contForward)
    print('Set continuous servos to backward')
elif direction == 's':
    pwm.setPWM(cont1Channel, 0, contStop)
    pwm.setPWM(cont2Channel, 0, contStop)
    print('Set continuous servos to stop')
else:
    print('"' + direction + '" is not a proper command. Use either f (forward),b (back) or s (stop).')