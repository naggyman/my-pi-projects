#!/usr/bin/python
# Code from Adafruit and Morgan French-Stagg

from Adafruit_PWM_Servo_Driver import PWM
from Simpler_Servo import Servo
from time import sleep
import sys

print('Continuous Servo Driver - Morgan French-Stagg')

channel = 2 #Address on PWM Driver (0 to 15)
pwm = PWM(address=0x40, debug=True) #Define pwm address
servo = Servo(pwm, channel) #Define Servo
x = int(sys.argv[1]) #Get degrees from Argument

# Left 120, mid 70, right 0

pwm.setPWMFreq(50) #Frequency to 50 hertz
servo.move_to_deg(x) #Move servo to specified degrees
